# NGINX log parse

### Пример лога
```
195.77.230.188 - - [02/Mar/2018:16:05:50 +0000] "GET /some-url HTTP/1.1" 200 1793 0.009 "https://example.com" "Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36" [upstream: 127.0.0.1:8000 200] request_id=-
```

### Формат лога
```
log_format mylog       '$remote_addr - $remote_user [$time_local] '
                       '"$request" $status $body_bytes_sent $request_time '
                       '"$http_referer" "$http_user_agent" '
                       '[upstream: $upstream_addr $upstream_status] '
                       'request_id=$upstream_http_x_request_id';
```

1. **Скрипт-однострочник на bash, который бы выводил ip, с которого пришло больше всего запросов**
```bash
$ cut -d' ' -f1 nginx.log | sort | uniq -c | sort -k1,1 | tail -1
```

2. **Скрипт на любом знакомом языке программирования или bash, который бы вывел top-10 ip, на которые было отдано больше всего трафика (`$body_bytes_sent` в логе). Сортировать по убыванию трафика.**
```bash
$ for ip in $(cut -d' ' -f1 nginx.log | sort | uniq); do fgrep -w "$ip" nginx.log | awk '{body_bytes_sent += $10}END{print $1, body_bytes_sent}'; done | sort -nrk 2,2 | head
```